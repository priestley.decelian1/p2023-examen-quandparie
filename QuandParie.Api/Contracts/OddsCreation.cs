﻿using System;
using System.ComponentModel.DataAnnotations;

namespace QuandParie.Api.Contracts
{
    public record OddsCreation(
        [Required] string Match,
        [Required] string Condition,
        [Required][Range(1, 100)] decimal Value);
}
