﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Services
{
    public class FakeIdentityProofer : IIdentityProofer
    {
        public bool Validates(Customer customer, byte[] identityProof)
            => identityProof.Length == 1 && identityProof[0] == customer.LastName[0];
    }
}
