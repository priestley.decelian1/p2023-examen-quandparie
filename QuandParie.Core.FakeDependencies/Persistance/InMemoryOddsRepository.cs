﻿using QuandParie.Core.Domain;
using QuandParie.Core.Persistance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.FakeDependencies.Persistance
{
    public class InMemoryOddsRepository : IOddsRepository
    {
        public Dictionary<Guid, Odds> Odds = new();

        public Task<Odds> GetAsync(Guid oddsId)
        {
            if (!Odds.TryGetValue(oddsId, out var result))
                result = null;
            return Task.FromResult(result);
        }

        public Task<IReadOnlyList<Odds>> GetForMatchAsync(string match)
        {
            IReadOnlyList<Odds> result = Odds.Values
                .Where(odds => odds.Match == match)
                .ToList();
            return Task.FromResult(result);
        }

        public void Save(Odds odds)
            => Odds[odds.Id] = odds;

        public Task SaveAsync(Odds odds)
        {
            Save(odds);
            return Task.CompletedTask;
        }
    }
}
