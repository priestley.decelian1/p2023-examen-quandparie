﻿using QuandParie.Core.Gateway.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuandParie.Core.Gateway
{
    public interface IBankingSystem
    {
        Task<ReceivedPaymentData> Receive(string paymentToken);
        Task Transfer(WireTransferData transfer);
    }
}
